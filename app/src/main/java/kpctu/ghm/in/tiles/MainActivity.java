package kpctu.ghm.in.tiles;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Icon;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

import static android.view.View.LAYER_TYPE_SOFTWARE;

public class MainActivity extends AppCompatActivity {

    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageView = (ImageView) findViewById(R.id.image);
        Paint paint = new Paint();
        paint.setColor(Color.WHITE);
        paint.setTextSize(22f);
        paint.setAntiAlias(true);
        paint.setFakeBoldText(true);
        paint.setStyle(Paint.Style.FILL);
        paint.setTextAlign(Paint.Align.LEFT);

        Bitmap myBitmap = Bitmap.createBitmap( 32, 32, Bitmap.Config.ARGB_8888 );
        myBitmap.setHasAlpha(true);

        //BitmapDrawable drawable = new BitmapDrawable(getResources(), myBitmap);
        //drawable.setAlpha(100);


        Canvas canvas = new Canvas();
        canvas.setBitmap(myBitmap);
        canvas.drawColor(Color.TRANSPARENT);
        canvas.drawText(String.format("Hi"), 0, 30, paint);

        Icon newIcon = Icon.createWithBitmap(myBitmap);

        imageView.setImageIcon(newIcon);
    }
}
