package kpctu.ghm.in.tiles;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Icon;
import android.net.TrafficStats;
import android.net.Uri;
import android.service.quicksettings.Tile;
import android.service.quicksettings.TileService;
import android.util.Log;
import android.widget.Toast;

import java.util.Locale;

public class MyService extends TileService {
    private static final String TAG = "Tile";

    @Override
    public void onTileAdded() {
        Log.i(TAG, "Method: onTileAdded()");
        super.onTileAdded();
    }

    @Override
    public void onTileRemoved() {
        super.onTileRemoved();
        Log.i(TAG, "Method: onTileRemoved()");
    }

    @Override
    public void onStartListening() {
        super.onStartListening();
        changeTileState();
        updateTile();
        Log.i(TAG, "Method: onStartListening()");
    }

    @Override
    public void onStopListening() {
        super.onStopListening();
        Log.i(TAG, "Method: onStopListening()");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.i(TAG, "Method: onCreate()");
    }

    @Override
    public void onClick() {
        super.onClick();
        Log.d(TAG, "onClick: " + Tile.STATE_ACTIVE + "  " + Tile.STATE_INACTIVE + "  " + Tile.STATE_UNAVAILABLE );
        Log.i(TAG, "Tile State: " + getQsTile().getState());

        if (!isLocked()) {
            updateTile();
        } else {
            unlockAndRun(new Runnable() {
                @Override
                public void run() {
                    updateTile();
                }
            });
        }
    }

    private void changeTileState() {
        Tile tile = this.getQsTile();

        if(tile.getState() == Tile.STATE_INACTIVE) {
            tile.setState(Tile.STATE_ACTIVE);
        }
    }

    private String getKb(float data) {
        return String.format(java.util.Locale.US,"%.1f", (data/1024));
    }

    private String getMb(float data) {
        return String.format(java.util.Locale.US,"%.1f", ((data/1024)/1024));
    }

    private String getGb(float data) {
        return String.format(java.util.Locale.US,"%.1f", (((data / 1024) / 1024) /1024));
    }

    private void updateTile() {

        Tile tile = this.getQsTile();

        Icon newIcon;
        String newLabel;
        int newState;

        float mobile = TrafficStats.getMobileRxBytes() + TrafficStats.getMobileTxBytes();
        float total = TrafficStats.getTotalRxBytes() + TrafficStats.getTotalTxBytes();

        if( mobile < 1024)  {
            newLabel = String.format(mobile + " Bytes");
        } else {
            if( (mobile > 1024) && ((mobile/1024) < 1024) ) {
                newLabel = String.format(getKb(mobile) + "Kb");
            } else {
                if( ((mobile /1024) /1024) > 1024 ) {
                    newLabel = String.format(getGb(mobile) + " Gb");
                } else {
                    newLabel = String.format(getMb(mobile) + " Mb");
                }
            }
        }

        Paint paint = new Paint();
        paint.setColor(Color.BLACK);
        paint.setTextSize(22f);
        paint.setAntiAlias(true);
        paint.setFakeBoldText(true);
        paint.setShadowLayer(6f, 0, 0, Color.BLACK);
        paint.setStyle(Paint.Style.FILL);
        paint.setTextAlign(Paint.Align.LEFT);

        Bitmap myBitmap = Bitmap.createBitmap( 32, 32, Bitmap.Config.ARGB_8888 );
        myBitmap.setHasAlpha(true);


        Canvas canvas = new Canvas(myBitmap);
        canvas.drawText(newLabel, 0, 32, paint);

        newIcon = Icon.createWithBitmap(myBitmap);


        //newIcon = Icon.createWithResource(getApplicationContext(), R.drawable.add);

        newState = Tile.STATE_ACTIVE;

        // Change the UI of the tile.
        tile.setLabel(newLabel);
        tile.setIcon(newIcon);
        tile.setState(newState);

        // Need to call updateTile for the tile to pick up changes.
        tile.updateTile();
    }


    private void changeTileState(int newState) {
        getQsTile().setIcon(Icon.createWithResource(MyService.this, newState == Tile.STATE_INACTIVE ? R.mipmap.ic_launcher : R.mipmap.ic_launcher));
        getQsTile().setState(newState);
        getQsTile().updateTile();
    }

}
